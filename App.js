import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import GlobalStyles from './src/global-style/GlobalStyle';
import AcademyInfo from './src/screens/academy-info/AcademyInfo';
import AddNewWorkout from './src/screens/add-new-workout/AddNewWorkout';
import ActivityName from './src/screens/activity-name/ActivityName';
import SearchExercise from './src/screens/search-exercise/SearchExercise';
import Home from './src/screens/home/Home';
import Colors from './src/global-style/Colors';
import Fonts from './src/global-style/Fonts';
import FontSize from './src/global-style/FontSize';

const Stack = createStackNavigator();
const App = () => {
  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={GlobalStyles.safeAreaStyle} />
      <View style={GlobalStyles.container}>
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerStyle: {
                backgroundColor: Colors.primary,
                // borderBottomColor: 'transparent',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
                // fontWeight: 'bold',
                fontFamily: Fonts.bold,
                // fontSize: FontSize.extraLargeXX
              },
            }}>
            <Stack.Screen
              name="Home"
              component={Home}
              options={{
                title: 'Home'
              }} />
            <Stack.Screen
              name="AcademyInfo"
              component={AcademyInfo}
              options={{
                title: 'Academy Info'
              }} />
            <Stack.Screen
              name="ActivityName"
              component={ActivityName} 
              options={{
                title: 'Activity Name'
              }}
              />
            <Stack.Screen
              name="SearchExercise"
              component={SearchExercise} 
              options={{ headerShown: false }}
              />
            <Stack.Screen
              name="AddNewWorkout"
              component={AddNewWorkout}
              options={{
                title: 'Add New Workout'
              }} />
          </Stack.Navigator>

        </NavigationContainer>
      </View>
      <SafeAreaView style={GlobalStyles.safeAreaStyle} />
    </>
  );
};
export default App;
