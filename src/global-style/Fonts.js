export default Fonts = {
    boldItalic: "Calibri-BoldItalic",
    bold: "Calibri-Bold",
    italic: "Calibri-Italic",
    lightItalic: "Calibri-LightItalic",
    light: "Calibri-Light",
    regular: "Calibri",
  };