import { StyleSheet } from "react-native";
import Colors from "./Colors";
import Fonts from "./Fonts";
import FontSize from "./FontSize";

const GlobalStyles = StyleSheet.create({
    safeAreaStyle:{
        backgroundColor:Colors.background
    },
    container:{
        flex:1,
        backgroundColor:Colors.background
    },
    labelStyle:{
        color:Colors.white,
        fontSize:FontSize.medium,
        fontFamily:Fonts.regular
    }
})

export default GlobalStyles