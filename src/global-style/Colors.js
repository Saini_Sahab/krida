const Colors = {
    primary: "#231F20",
    background: "#231F20",
    green: "#05AD51",
    blue:"#0086B6",
    white: "#ffffff",
    black: "#000000",
    red:"#F9232F",
    inputBackground:"#353535",
    dashColor:"#707070",
    placeHolderColor:"#707070",
}

export default Colors;