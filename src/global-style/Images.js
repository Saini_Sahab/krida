export default Images = {
    ic_camera: require('../assets/images/ic_camera.png'),
    ic_close: require('../assets/images/ic_close.png'),
    ic_plus: require('../assets/images/ic_plus.png'),
    ic_activity: require('../assets/images/ic_activity.png'),
    ic_right_arrow: require('../assets/images/ic_right_arrow.png'),
    logo_black: require('../assets/images/logo_black.png'),
}