export default FontSize = {
    extraSmall : 10,
    small : 12,
    regular : 14,
    medium : 16,
    large : 18,
    extraLarge : 20,
    extraLargeX : 22,
    extraLargeXX : 24,
}