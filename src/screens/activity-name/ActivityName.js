import { View,ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react';
import GlobalStyles from '../../global-style/GlobalStyle'
import TextInputLabel from '../../components/TextInputLabel'
import ActivityStats from '../../components/ActivityStats'
import ActivityItem from '../../components/ActivityItem';
import styles from './Styles'

export default function AddNewWorkout() {
    const [activities, setActivities] = useState([{ id: 1, name: "Lunges", complete: 90 }, { id: 2, name: "Lunges", complete: 50 }, { id: 3, name: "Lunges", complete: 60 }, { id: 4, name: "Lunges", complete: 30 }, { id: 5, name: "Lunges", complete: 80 }, { id: 6, name: "Lunges", complete: 90 }])
    useEffect(() => {

    })
    return (
        <View style={GlobalStyles.container}>
            <ScrollView style={styles.scrollStyle}>
                <View style={styles.stats}>
                    <TextInputLabel label={"ACTIVITY STATS"} style={styles.generalLabel} />
                    <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center', marginTop: 20 }}>
                        <ActivityStats title={"Workout Time"} unit={32} unitType={"min"} />
                        <ActivityStats title={"Calories Burned"} unit={123} unitType={"calories"} />
                        <ActivityStats title={"Points Earned"} unit={100} unitType={"points"} />
                    </View>
                </View>
                <View style={{ marginTop: 20 }} />
                {
                    activities && activities.map((item)=>{
                        return <ActivityItem key={item.id} title={item.id + '. ' + item.name} status={item.complete +"% perfectly done"} progress={item.complete}/>
                    })
                }
                
            </ScrollView>

        </View>
    )
}

