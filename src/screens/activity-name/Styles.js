import { StyleSheet } from 'react-native'
import Fonts from '../../global-style/Fonts'
import FontSize from '../../global-style/FontSize'
import Colors from '../../global-style/Colors'

const Styles = StyleSheet.create({
    scrollStyle: {
        paddingTop: 10,
        paddingHorizontal: 15
    },
    generalLabel: {
        fontSize: FontSize.regular,
        fontFamily:Fonts.bold
    },
    stats:{
        backgroundColor: Colors.inputBackground,
        paddingTop: 15,
        paddingBottom: 25,
        paddingHorizontal: 20,
        borderRadius: 15,
    }
})

export default Styles