import { StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        padding: 12,
        marginTop: 15,
        width: '80%'
    }
})

export default Styles