import React from 'react'
import { View } from 'react-native'
import Button from '../../components/Button'
import GlobalStyles from '../../global-style/GlobalStyle'
import styles from './Styles'

export default function Home({ navigation }) {
    return (
        <View style={[GlobalStyles.container, styles.container]}>
            <Button text={"Academy Info"} style={styles.button} onPress={() => { navigation.navigate('AcademyInfo') }} />
            <Button text={"Add New Workout"} style={styles.button} onPress={() => { navigation.navigate('AddNewWorkout') }}/>
            <Button text={"Search Exercise"} style={styles.button} onPress={() => { navigation.navigate('SearchExercise') }}/>
            <Button text={"Activity Name"} style={styles.button} onPress={() => { navigation.navigate('ActivityName') }}/>
        </View>
    )
}

