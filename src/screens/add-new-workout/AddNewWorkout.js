import { View, ScrollView } from 'react-native'
import React, { useState } from 'react';
import GlobalStyles from '../../global-style/GlobalStyle'
import TextInputLabel from '../../components/TextInputLabel'
import TextInputView from '../../components/TextInputView'
import Button from '../../components/Button'
import ImageView from '../../components/ImageView'
import styles from './Styles'

export default function AddNewWorkout() {
    const [image, setImage] = useState(null)
    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")

    return (
        <View style={GlobalStyles.container}>
            <ScrollView style={styles.scrollStyle}>
                <TextInputLabel label={"GENERAL DETAILS"} style={styles.generalLabel} />
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <TextInputLabel label={"Choose a cover pic"} style={styles.coverPic} />
                    <ImageView img={image ? { uri: image } : null} type={image ? 3 : 2} size={styles.imageView} cameraImageSize={styles.cameraImageSize} />
                </View>
                <TextInputLabel label={"Title"} style={styles.label} />
                <TextInputView
                    placeHolder={"Give your workout a title"}
                    currentLength={title.length}
                    maxLength={25}
                    onChangeText={(text) => setTitle(text)}
                />
                <TextInputLabel label={"Description"} style={styles.label} />
                <TextInputView
                    placeHolder={"Description the workout in brief"}
                    currentLength={description.length}
                    isMultiline={true}
                    maxLength={80}
                    onChangeText={(text) => setDescription(text)}
                />
                <Button text={"Next"} style={styles.button} />
            </ScrollView>

        </View>
    )
}

