import { StyleSheet } from 'react-native'
import Fonts from '../../global-style/Fonts'
import FontSize from '../../global-style/FontSize'

const Styles = StyleSheet.create({
    imageView: {
        height: 150,
        width: 180,
    },
    cameraImageSize: {
        height: 40,
        width: 40,
    },
    generalLabel: {
        marginLeft:15,
        fontSize: FontSize.regular,
        fontFamily:Fonts.bold
    },
    label: {
        marginTop: 20,
        marginBottom: 15,
    },
    scrollStyle: {
        paddingTop: 20,
        paddingHorizontal: 15
    },
    button: {
        width: 100,
        padding: 12,
        marginVertical: 30,
        marginHorizontal:15,
        alignSelf: 'flex-end'
    },
    coverPic:{ 
        marginTop: 20,
        marginBottom:10, 
        fontSize: FontSize.large, 
        fontFamily: Fonts.bold 
    }
})

export default Styles