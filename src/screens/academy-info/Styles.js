import { StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
    label: {
        marginTop: 20,
        marginBottom: 15,
    },
    scrollStyle: {
        paddingTop: 10,
        paddingHorizontal: 15
    },
    button: {
        padding: 12,
        margin: 15,
    },
    photos:{
        flexDirection: 'row',
        marginTop:10,
        marginBottom:30,
        flex: 1,
        justifyContent: 'space-between'
    }
})

export default Styles