import { View, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react';
import GlobalStyles from '../../global-style/GlobalStyle'
import TextInputLabel from '../../components/TextInputLabel'
import TextInputView from '../../components/TextInputView'
import Button from '../../components/Button'
import ImageView from '../../components/ImageView'
import styles from './Styles'
import Images from '../../global-style/Images';

export default function AcademyInfo() {
    const [image1, setImage1] = useState(Images.logo_black)
   
    return (
        <View style={GlobalStyles.container}>
            <ScrollView style={styles.scrollStyle}>
                <TextInputLabel label={"Academy Name"} style={styles.label} />
                <TextInputView placeHolder={"Old Academy Name"} />
                <TextInputLabel label={"Description"} style={styles.label} />
                <TextInputView placeHolder={"Add an academy description.."} isMultiline={true} />
                <TextInputLabel label={"Academy Photos"} style={styles.label} />
                <View style={styles.photos}>

                    <ImageView img={Images.logo_black} type={3} />
                    <ImageView img={null} type={2} />
                    <ImageView img={null} type={1} />
                </View>
            </ScrollView>
            <Button text={"Save Changes"} style={styles.button} />
        </View>
    )
}

