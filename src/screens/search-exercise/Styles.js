import { StyleSheet } from 'react-native'
import Fonts from '../../global-style/Fonts'
import FontSize from '../../global-style/FontSize'
import Colors from '../../global-style/Colors'

const Styles = StyleSheet.create({
    scrollStyle: {
        paddingTop: 10,
        paddingHorizontal: 15
    },
    search:{
        backgroundColor: Colors.inputBackground,
        flexDirection: 'row',
        paddingHorizontal:10,
        alignItems:'center',
    },
    searchClose:{
        height: 30,
        width: 30,
        padding:10,
        resizeMode: 'cover',
        alignSelf: 'center',
        tintColor:Colors.placeHolderColor
    },
    searchInput:{
        flex: 1,
        marginLeft:20,
        color: Colors.white,
        fontFamily:Fonts.regular,
        fontSize:FontSize.extraLarge
    }
})

export default Styles