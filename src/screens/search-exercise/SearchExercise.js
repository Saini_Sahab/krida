import { View, ScrollView, TouchableOpacity, TextInput,Image } from 'react-native'
import React, { useState, useEffect } from 'react';
import GlobalStyles from '../../global-style/GlobalStyle'
import Images from '../../global-style/Images';
import Colors from '../../global-style/Colors';
import ExerciseItem from '../../components/ExerciseItem';
import styles from './Styles'

const initialActivities = [{ id: 1, name: "Exercise Name", complete: 90 }, { id: 2, name: "Exercise Name", complete: 50 }, { id: 3, name: "Lunges", complete: 60 }, { id: 4, name: "Lunges", complete: 30 }, { id: 5, name: "Lunges", complete: 80 }, { id: 6, name: "Lunges", complete: 90 }];
export default function SearchExercise({navigation}) {
    const [activities, setActivities] = useState(initialActivities)
    const [filteredActivities, setFilteredActivities] = useState([])
    const [searchTerm, setSearchTerm] = useState("");

    useEffect(()=>{
        if(!searchTerm){
            setFilteredActivities(activities)
        }else{
            const results = activities.filter(el => el.name.toLowerCase().includes(searchTerm.toLowerCase()));
            setFilteredActivities(results)
        }
    },[searchTerm])
    
    return (
        <View style={GlobalStyles.container}>
            <View style={styles.search}>
                <TouchableOpacity
                 onPress={() => { !searchTerm? navigation.goBack():setSearchTerm("")}}>
                    <Image source={Images.ic_close} style={styles.searchClose}/>
                </TouchableOpacity>
                <TextInput
                    placeholder={"Search Exercises"}
                    selectionColor={Colors.white}
                    autoCorrect={false}
                    placeholderTextColor={Colors.placeHolderColor}
                    value={searchTerm}
                    onChangeText={text => { setSearchTerm(text) }}
                    style={styles.searchInput}
                />
            </View>
            <ScrollView style={styles.scrollStyle}>
                <View style={{ marginTop: 15 }} />
                {
                    filteredActivities && filteredActivities.map((item, index) => {
                        return <ExerciseItem key={item.id} title={item.name} status={item.complete + "% perfectly done"} progress={item.complete} />
                    })
                }

            </ScrollView>

        </View>
    )
}

