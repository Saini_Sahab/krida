import React from 'react'
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native'
import Colors from '../global-style/Colors'
import Images from '../global-style/Images'

const ImageView = (props) => {
    return (
        <View >
            {
                props.type == 1 && <View style={[styles.viewBlank,styles.imageSize]} />
            }
            {
                props.type == 2 &&
                <TouchableOpacity style={[styles.viewCamera,styles.imageSize, props.size]}>
                    <Image source={Images.ic_camera} style={[styles.cameraImage,props.cameraImageSize]} />
                </TouchableOpacity>
            }
            {
                props.type == 3 &&
                <View style={[styles.viewImage, props.size]}>
                    <Image source={props.img} style={[styles.imageSize,{resizeMode:'cover',borderRadius: 5,},props.size]} />
                    <TouchableOpacity style={styles.cross}>
                        <Image source={Images.ic_close} style={styles.crossImage} />
                    </TouchableOpacity>
                </View>
            }


        </View>
    )
}

export default ImageView

const styles = StyleSheet.create({
    imageSize: {
        height: 100,
        width: 100,
    },
    viewBlank: {
        borderStyle: 'dashed',
        borderRadius: 5,
        borderWidth: 2.5,
        borderColor: Colors.dashColor
    },
    viewCamera: {
        backgroundColor: Colors.inputBackground,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    viewImage: {
        backgroundColor: Colors.inputBackground,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    cross: {
        width: 24,
        height: 24,
        backgroundColor: Colors.red,
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: -12,
        right: -12,
    },
    cameraImage: { height: 30, width: 30, resizeMode: 'contain', },
    crossImage:{ height: 22, width: 22, resizeMode: 'contain' }
})