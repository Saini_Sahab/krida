import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import FontSize from '../global-style/FontSize'
import TextInputLabel from './TextInputLabel'

const ActivityStats = (props) => {
    return (
        <View>
            <TextInputLabel label={props.title} style={styles.title} />
            <TextInputLabel label={props.unit} style={styles.unit} />
            <TextInputLabel label={props.unitType} style={styles.unitType} />
        </View>
    )
}

export default ActivityStats

const styles = StyleSheet.create({
    title:{
        fontSize:FontSize.small,
        alignSelf:'center',
    },
    unit:{
        fontSize:25,
        alignSelf:'center',
        marginTop:10
    },
    unitType:{
        fontSize:FontSize.small,
        alignSelf:'center',
        marginTop:8,
    }
})