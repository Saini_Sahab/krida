import React from 'react'
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import Colors from '../global-style/Colors'
import FontSize from '../global-style/FontSize'
import Images from '../global-style/Images'
import TextInputLabel from './TextInputLabel'
import Fonts from '../global-style/Fonts'


export default function ExerciseItem(props) {
    return (
        <TouchableOpacity style={styles.touchable}>
            <Image source={Images.ic_activity} style={styles.activityImage} />
            <View style={{
                flex: 1,
                flexDirection: 'column',
                marginLeft: 30,
                marginRight: 20,
                justifyContent: 'center'
            }}>
                <TextInputLabel label={props.title} style={styles.title} />
            </View>
            <Image source={Images.ic_plus} style={styles.arrow} />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    touchable: {
        backgroundColor: Colors.inputBackground,
        borderRadius: 8,
        padding: 10,
        flexDirection: 'row',
        flex: 1,
        marginBottom: 10
    },
    activityImage: { height: 55, width: 55, borderRadius: 10, resizeMode: 'cover' },
    title: {
        fontSize: FontSize.large,
        color: Colors.white,
        fontFamily: Fonts.bold
    },
    arrow: {
        height: 24,
        width: 24,
        resizeMode: 'cover',
        alignSelf: 'center',
        marginRight:20,
    }
})