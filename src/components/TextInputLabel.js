import React from 'react'
import { View, Text } from 'react-native'
import GlobalStyles from '../global-style/GlobalStyle'

const TextInputLabel = (props) => {
    return (
        <View>
            <Text style={[GlobalStyles.labelStyle,props.style]}>{props?.label}</Text>
        </View>
    )
}

export default TextInputLabel