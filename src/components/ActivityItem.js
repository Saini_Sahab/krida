import React from 'react'
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native'
import Colors from '../global-style/Colors'
import FontSize from '../global-style/FontSize'
import Images from '../global-style/Images'
import TextInputLabel from './TextInputLabel'
import ProgressBar from 'react-native-progress/Bar';


export default function ActivityItem(props) {
    return (
        <TouchableOpacity style={styles.touchable}>
            <Image source={Images.ic_activity} style={styles.activityImage} />
            <View style={{
                flex: 1,
                flexDirection: 'column',
                marginHorizontal: 20,
            }}>
                <TextInputLabel label={props.title} style={styles.title} />
                <ProgressBar progress={props.progress/100} width={200} color={Colors.red} style={styles.progress}/>
                <TextInputLabel label={props.status} style={styles.status} />
            </View>
            <Image source={Images.ic_right_arrow} style={styles.arrow} />

        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    touchable:{
        backgroundColor: Colors.inputBackground,
        borderRadius: 8,
        padding: 10,
        flexDirection: 'row',
        flex: 1,
        marginBottom:10
    },
    activityImage:{ height: 55, width: 55, borderRadius: 10, resizeMode: 'cover' },
    progress:{marginTop:5,height:6, borderRadius:5,borderColor:'transparent',backgroundColor:Colors.placeHolderColor},
    title: {
        fontSize: FontSize.extraLarge,
        color: Colors.white,
        fontFamily:Fonts.bold
    },
    status:{
        fontSize:FontSize.small,
        marginTop:5,
        color:Colors.placeHolderColor
    },
    arrow:{ height: 30, width: 30, resizeMode: 'cover', alignSelf: 'center' }
})