import { View, Text, StyleSheet, TextInput } from 'react-native'
import React from 'react';
import Colors from '../global-style/Colors';
import FontSize from '../global-style/FontSize';
import Fonts from '../global-style/Fonts';

const TextInputView = (props) => {
    return (
        <View style={styles.container}>
            <TextInput
                selectionColor={Colors.white}
                numberOfLines={props.isMultiline ? 5 : 1}
                textAlignVertical={'top'}
                multiline={props.isMultiline}
                maxLength={props.maxLength}
                placeholder={props.placeHolder}
                placeholderTextColor={Colors.placeHolderColor}
                autoCorrect={false}
                onChangeText={text => { if (props.onChangeText) props.onChangeText(text) }}
                style={[styles.textInput, props.isMultiline ? { height: 100 } : {}]} />
            {
                props.maxLength && <Text style={[styles.text, props.isMultiline ? { alignSelf: 'flex-end', } : {}]}>{((props.currentLength) ? props.currentLength : 0) + '/' + props.maxLength}</Text>
            }
        </View>
    )
}

export default TextInputView

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: Colors.inputBackground,
        color: Colors.white,
        borderRadius: 8,
    },
    textInput: {
        flex: 1,
        color: Colors.white,
        fontSize: FontSize.large,
        fontFamily: Fonts.regular,
        paddingVertical: 10,
        paddingHorizontal: 15
    },
    text: {
        alignSelf: 'center',
        color: Colors.placeHolderColor,
        fontFamily: Fonts.regular,
        paddingVertical: 10,
        paddingHorizontal: 15
    }
})