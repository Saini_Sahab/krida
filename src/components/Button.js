import React from 'react'
import { Text, StyleSheet, TouchableOpacity } from 'react-native'
import Colors from '../global-style/Colors';
import Fonts from '../global-style/Fonts';
import FontSize from '../global-style/FontSize';

const Button = (props) => {
    return (
        <TouchableOpacity elevation={5} style={[styles.button, props.style]} onPress={props.onPress}>
            <Text style={styles.text}>{props.text}</Text>
        </TouchableOpacity>

    )
}

export default Button;

const styles = StyleSheet.create({
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.red,
        borderRadius: 5,
    },
    text: {
        color: Colors.white,
        fontSize: FontSize.large,
        fontFamily: Fonts.light,
    }
})
